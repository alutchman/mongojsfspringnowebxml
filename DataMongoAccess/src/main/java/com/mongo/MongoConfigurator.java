package com.mongo;

import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.SimpleMongoDbFactory;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@Configuration
@ComponentScan(
        basePackages = {"com.mongo"}
)
@PropertySource(value = { "classpath:application.properties" })
@EnableMongoRepositories
public class MongoConfigurator {

    @Autowired
    private ApplicationContext applicationContext;


    @Value("${mongo.host}")
    private String mongoHost ;

    @Value("${mongo.port}")
    private int mongoPort ;


    @Value("${mongo.dbname}")
    private String mongoDbName ;



    @Bean
    public MongoClient mongo() throws Exception {
        return new MongoClient( mongoHost , mongoPort );
    }

    @Autowired
    @Bean
    public MongoDbFactory mongoDbFactory(MongoClient mongo) throws Exception {
        return new SimpleMongoDbFactory(mongo,mongoDbName);
    }

    @Autowired
    @Bean
    public MongoTemplate mongoTemplate(MongoDbFactory mongoDbFactory) throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(mongoDbFactory);
        return mongoTemplate;

    }


}