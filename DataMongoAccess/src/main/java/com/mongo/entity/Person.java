package com.mongo.entity;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.List;

@Document(collection="person")
public class Person {

		private static final long serialVersionUID = -7379899596269999942L;

		@Id
		private Long personId;
		
		private String lastName;

	    private String firstName;
		
		private int age;
		
		@DBRef(db="address")
		private List<Address> addresses = new ArrayList<>();
		
		public Person()
		{}
		
		
		@PersistenceConstructor
		public Person(Long personId, String lastName, String firstName,int age)
		{
				super();
				this.personId = personId;
				this.lastName = lastName;
				this.firstName = firstName;
				this.age = age;
		}

		public Long getPersonId()
		{
				return personId;
		}

		public void setPersonId(Long personId)
		{
				this.personId = personId;
		}



		public int getAge()
		{
				return age;
		}

		public void setAge(int age)
		{
				this.age = age;
		}

		public List<Address> getAddresses()
		{
				return addresses;
		}

		public String getLastName() {
			return lastName;
		}

		public void setLastName(String lastName) {
			this.lastName = lastName;
		}

		public String getFirstName() {
			return firstName;
		}

		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}

		public void setAddresses(List<Address> addresses) {
				this.addresses = addresses;
		}


		@Override
		public String toString() {
				return "Person [personId=" + personId + ", firstName=" + firstName +" " +
						", age=" + age + ", addresses=" + addresses + "]";
		}


		
}
