package com.mongo.entity;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="User")
public class User extends BaseEntity{

    private static final long serialVersionUID = 4920555965474452778L;

    private String login;
    private byte[] password;

    private DateTime createdDate;

    private LocalDateTime birthDate;



    public LocalDateTime getBirthDate() {
        return birthDate;
    }


    public User(){
        setCreatedDate(new DateTime());
    }

    public void setBirthDate(LocalDateTime birthDate) {
        this.birthDate = birthDate;
    }

    public byte[] getPassword() {
        return password;
    }

    public void setPassword(byte[] password) {
        this.password = password;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public DateTime getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(DateTime createdDate) {
        this.createdDate = createdDate;
    }


    @Override
    public String toString() {
        String result = "{"+
                "login:"+ login;
        result +=  "}";
        return result;
    }
}
