package com.mongo.repo;


import com.mongo.entity.Address;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AddressRepo extends MongoRepository<Address, Long>
{
}
