package com.mongo.repo;

import com.mongo.entity.Counter;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CounterRepo extends MongoRepository<Counter, Long> {

}
