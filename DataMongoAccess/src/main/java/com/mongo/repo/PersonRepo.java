package com.mongo.repo;


import com.mongo.entity.Person;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface PersonRepo extends MongoRepository<Person, Long>  {
	@Query("{'lastName' : ?0}")
	public List<Person> searchByName(String personName);

	@Query("{'firstName' : ?0, 'lastName' : ?1}")
	public List<Person> searchByFullName(String firstName, String lastName);


	@Query("{'firstName' : ?0, 'lastName' : ?1}")
	public Person findOne(String firstName, String lastName);

}
