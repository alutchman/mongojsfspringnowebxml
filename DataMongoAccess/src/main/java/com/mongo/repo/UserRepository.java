package com.mongo.repo;

import com.mongo.entity.User;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    @Query("{'login' : ?0}")
    List<User> findAllByLogin(String login);

    @Query("{'login' : ?0}")
    User findByLogin(String login);

}
