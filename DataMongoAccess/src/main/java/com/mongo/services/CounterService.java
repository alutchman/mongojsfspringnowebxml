package com.mongo.services;

import com.mongo.entity.Counter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

@Service
public class CounterService {
    @Autowired
    private MongoOperations mongo;

    private Long getNextSequence(String collectionName) {
        Counter counter = mongo.findAndModify(
                query(where("_id").is(collectionName)),
                new Update().inc("seq", 1),
                options().returnNew(true),
                Counter.class);

        if (counter == null) {
            counter = new Counter();
            counter.setId(collectionName);
            counter.setSeq(1L);
            mongo.save(counter);
        }
        return counter.getSeq();

    }


    public <T> Long getNextSequence(Class<T> classz) {
        String collectionName = classz.getSimpleName();
        Document table = classz.getAnnotation(Document.class);
        if (table != null) {
            collectionName = table.collection();
        }
       return getNextSequence(collectionName);

    }

}
