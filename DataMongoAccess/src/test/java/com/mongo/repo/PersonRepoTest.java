package com.mongo.repo;

import com.mongo.entity.Person;
import com.mongo.services.CounterService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


@SuppressWarnings("SpringJavaAutowiringInspection")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringMongoConfig.class})
public class PersonRepoTest {

    static final int QTY = 20;
    static final String[] FIRST_NAME;
    static final String[] LAST_NAME ;
    static {
        List<String> firstNames = new ArrayList<>();
        List<String> lastNames  = new ArrayList<>();
        for(int i =0; i<QTY; i++){
            firstNames.add("Voornaam"+ i);
            lastNames.add("Achternaam"+ i);

        }
        FIRST_NAME = firstNames.toArray(new String[0]);
        LAST_NAME  = lastNames.toArray(new String[0]);
    }



    @Autowired
    private PersonRepo personRepo;

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private CounterService counterService;

    @Autowired
    private CounterRepo counterRepo;



    @Before
    public void init() {

        personRepo.deleteAll();
        for (int i = 0; i < QTY; i++) {
            Person person = new Person();
            Long newId = counterService.getNextSequence(Person.class);
            person.setPersonId(newId);
            person.setFirstName(FIRST_NAME[i]);
            person.setLastName(LAST_NAME[i]);
            personRepo.save(person);

        }
    }


    @After
    public void after() {
        personRepo.deleteAll();
        counterRepo.deleteAll();
    }


    @Test
    public void testMongo(){
        List<Person> lusers =  personRepo.findAll();
        assertEquals(QTY, lusers.size());

        for (Person person : lusers) {
            assertTrue(person.getFirstName().startsWith("Voornaam"));
            assertTrue(person.getLastName().startsWith("Achternaam"));
        }
    }

    @Test
    public void testSearchByName() throws Exception {
        List<Person> lusers =  personRepo.searchByName("Achternaam19");
        assertEquals(1L, lusers.size());
        assertEquals("Voornaam19",lusers.get(0).getFirstName());
    }

    @Test
    public void testSearchByFullName() throws Exception {
        //searchByFullName
        List<Person> lusers =  personRepo.searchByFullName("Voornaam19", "Achternaam19");
        assertEquals(1L, lusers.size());
        assertEquals("Voornaam19",lusers.get(0).getFirstName());
    }



    @Test
    public void testDelete() throws Exception {
        //searchByFullName
        List<Person> lusers =  personRepo.searchByFullName("Voornaam19", "Achternaam19");
        assertEquals(1L, lusers.size());
        Person person = lusers.get(0);

        personRepo.delete(person);
        lusers =  personRepo.searchByFullName("Voornaam19", "Achternaam19");
        assertEquals(0L, lusers.size());

        List<Person> lusersAll =  personRepo.findAll();
        assertEquals(QTY-1, lusersAll.size());
    }




    @Test
    public void testUpdate() throws Exception {
        List<Person> lusers =  personRepo.searchByFullName("Voornaam19", "Achternaam19");
        assertEquals(1L, lusers.size());
        Person person = lusers.get(0);
        person.setLastName("ChangedName");

        personRepo.save(person);
        lusers =  personRepo.searchByFullName("Voornaam19", "Achternaam19");
        assertEquals(0L, lusers.size());

        lusers =  personRepo.searchByFullName("Voornaam19", "ChangedName");
        assertEquals(1L, lusers.size());
    }


    @Test
    public void testAdd() {
        List<Person> lusersAllBefore =  personRepo.findAll();

        assertEquals(QTY, lusersAllBefore.size());

        Person person = new Person();
        person.setPersonId(1L);
        person.setLastName("NewLastName");
        person.setFirstName("NewFirstName");

        Person personAdded = personRepo.save(person);

        List<Person> lusersAllAfter =  personRepo.findAll();

        assertEquals(QTY, lusersAllAfter.size());

        List<Person> lusers =  personRepo.searchByFullName("NewFirstName", "NewLastName");
        assertEquals(1L, lusers.size());


        lusers =  personRepo.searchByFullName("Voornaam0", "Achternaam0");
        assertEquals(0L, lusers.size());

    }

    @Test
    public void testFindOne()  {
        Person person = personRepo.findOne("Voornaam0", "Achternaam0");
        assertNotNull(person);

        Person person2 = personRepo.findOne("Voornaam99990", "Achternaam999990");
        assertNull(person2);
    }
}
