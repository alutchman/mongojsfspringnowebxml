package com.jsf.config;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@ComponentScan(
        basePackages = {"com.jsf","com.mongo"}
)
@EnableWebMvc
public class WebappConfig extends WebMvcConfigurerAdapter  {
    private static final Logger logger = Logger.getLogger(WebappConfig.class);

    @Autowired
    private ApplicationContext aplicationContext;

    @Bean
    public InternalResourceViewResolver setupViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/views/");
        resolver.setSuffix(".xhtml");

        return resolver;
    }





}