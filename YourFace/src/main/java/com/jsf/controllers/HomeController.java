package com.jsf.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.Map;

@Controller
public class HomeController {


	@RequestMapping(value="/")
	public ModelAndView test(){
		return new ModelAndView("index");
	}


	@RequestMapping(value="/home.html")
	public ModelAndView testHome(){
		Map<String, Object> datamap = new HashMap<String, Object>();
		datamap.put("date", "Vandaag");
		return new ModelAndView("home" ,datamap);
	}


	@RequestMapping(value="/newUser.html")
	public ModelAndView addUser(){

		return new ModelAndView("newUser");
	}


}
