package com.jsf.jsfbean;

import org.springframework.web.jsf.FacesContextUtils;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import java.io.Serializable;

public abstract class BaseJsfBean implements Serializable {

    @PostConstruct
    protected void init() {
        FacesContextUtils
                .getRequiredWebApplicationContext(FacesContext.getCurrentInstance())
                .getAutowireCapableBeanFactory().autowireBean(this);

        // All AutoWired stuff will now be available.....springBeanName is now available.
    }

}
