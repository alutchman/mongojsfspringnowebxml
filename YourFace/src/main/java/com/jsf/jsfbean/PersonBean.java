package com.jsf.jsfbean;

import com.mongo.repo.PersonRepo;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;


@ManagedBean
@SessionScoped
public class PersonBean extends BaseJsfBean {


    private static final long serialVersionUID = 814310865557460750L;

    private String firstName;
    private String lastName;
    private String output;


    @Autowired
    private PersonRepo personRepo;

    public String getInfo(){
        return "Info from Person bean";
    }


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String findUsers(){


        return "dffsfdsf";
    }

    public String getOutput() {
        return output;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public void submit(){
        output = "You submitted: " + firstName +  " and " + lastName;
    }



}
