package com.jsf.jsfbean;


import com.mongo.entity.User;
import com.mongo.repo.UserRepository;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.springframework.beans.factory.annotation.Autowired;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@ManagedBean
@SessionScoped
public class UserBean  extends BaseJsfBean {
    public static enum TableSort{
        LOGIN,CREATE,BIRTH
    }


    private String newUsername;


    private TableSort tableSort = TableSort.LOGIN;

    private Date birthDate;


    private boolean sortAscending = true;


    private static final long serialVersionUID = 3719324063235211888L;


    private List<User> users;



    @Autowired
    private UserRepository userRepository;





    public String printMsgFromSpring() {
        return "Your are getting this news flash from the UserBean at: "+ new Date();
    }



    public String getNewUsername() {
        return newUsername;
    }

    public void setNewUsername(String newUsername) {
        this.newUsername = newUsername;
    }


    public void submitNewUsername() {
        User checkUser =  userRepository.findByLogin(newUsername);
        if (checkUser == null && newUsername.trim().length() > 0) {
            //=== add it and reset
            checkUser = new User();
            checkUser.setLogin(newUsername);
            LocalDateTime localDateTime = LocalDateTime.fromDateFields(birthDate);
            checkUser.setBirthDate(localDateTime);

            checkUser.setCreatedDate(new DateTime());
            userRepository.save(checkUser);
        }
        newUsername = "";
        birthDate = null;

    }

    public List<User> getUsers() {
        users = userRepository.findAll();
        sortInit();
        return users;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }


    public void sortInit(){

        if (tableSort.equals(TableSort.LOGIN)) {
            Collections.sort(users, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {

                    if(sortAscending) {
                        return o1.getLogin().compareTo(o2.getLogin());
                    } else {
                        return o2.getLogin().compareTo(o1.getLogin());
                    }

                }
            });
        } else  if (tableSort.equals(TableSort.BIRTH)) {
            Collections.sort(users, new Comparator<User>() {

                @Override
                public int compare(User o1, User o2) {

                    if(sortAscending) {
                        return o1.getBirthDate().compareTo(o2.getBirthDate());
                    } else {
                        return o2.getBirthDate().compareTo(o1.getBirthDate());
                    }

                }
            });
        }   else  if (tableSort.equals(TableSort.CREATE)) {
            Collections.sort(users, new Comparator<User>() {

                @Override
                public int compare(User o1, User o2) {

                    if(sortAscending) {
                        if (o1 == null) return -1;
                        if (o2 == null) return -1;
                        return o1.getCreatedDate().compareTo(o2.getCreatedDate());
                    } else {
                        if (o1 == null) return 1;
                        if (o2 == null) return 1;
                        return o2.getCreatedDate().compareTo(o1.getCreatedDate());
                    }

                }
            });
        }

    }

    public void sortByName() {
        if (tableSort.equals(TableSort.LOGIN)) {
           sortAscending = !sortAscending;
        } else {
            sortAscending = true;
            tableSort = TableSort.LOGIN;
        }
    }



    public void sortByBirth() {
        if (tableSort.equals(TableSort.BIRTH)) {
            sortAscending = !sortAscending;
        } else {
            sortAscending = true;
            tableSort = TableSort.BIRTH;
        }
    }

    public void sortByCreation() {
        if (tableSort.equals(TableSort.CREATE)) {
            sortAscending = !sortAscending;
        } else {
            sortAscending = true;
            tableSort = TableSort.CREATE;
        }
    }

    public void clearAll(){
        userRepository.deleteAll();
    }



}
